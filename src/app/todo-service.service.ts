import { Headers, Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { ToDoTask } from './model/ToDoTask';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class TodoServiceService {
  private baseUrl = 'http://localhost:8080/SpringRestProject';
   headers: Headers = new Headers();

  constructor(private http: Http) { 
    // let headers = new HttpHeaders();
    this.createAuthorizationHeader(this.headers);
    this.headers.append('Accept', 'application/json');
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('charset', 'utf-8');
  //  set('Content-Type', 'application/json; charset=utf-8');
   console.log(this.headers)
  }

  createAuthorizationHeader(headers:Headers) {
    headers.append('Authorization', 'Basic ' +
      btoa('a20e6aca-ee83-44bc-8033-b41f3078c2b6:c199f9c8-0548-4be7-9655-7ef7d7bf9d20')); 
  }

  getToDoTasks():  Promise<ToDoTask[]>  {
    return this.http.get(this.baseUrl + '/task/')
      .toPromise()
      .then(response => response.json() as ToDoTask[])
      .catch(this.handleError);
     
  }

  updateTodo(todoTask: ToDoTask): Promise<ToDoTask> {
    return this.http.put(this.baseUrl + '/task/' + todoTask.taskID, todoTask,{headers:this.headers})
      .toPromise()
      .then(response => response.json() as ToDoTask)
      .catch(this.handleError);
  }

  deleteTask(todoTask: ToDoTask): Promise<any> {
    return this.http.delete(this.baseUrl + '/task/' + todoTask.taskID,{headers:this.headers})
      .toPromise()
      .catch(this.handleError);
  }

  createTodo(todoTask: ToDoTask){
   
    // let options = new RequestOptions({ headers: this.headers });
    return this.http.post(this.baseUrl + '/task/', JSON.stringify(todoTask),{headers:this.headers})
      .toPromise().then(response => 
        {
          console.log(response);
          if(response.ok)
            return response;
          else 
            throw new Error()
          // response.json() as ToDoTask;
        })
      .catch(this.handleError);

    


  }


  private handleError(error: any): Promise<any> {
    console.log('Some error occured', error);
    return Promise.reject(error.message || error);
  }

}
