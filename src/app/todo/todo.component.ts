import { TodoServiceService } from './../todo-service.service';
import { Component, OnInit } from '@angular/core';
import { ToDoTask } from '../model/ToDoTask';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  todoTasks:ToDoTask[]=[];
  todoTask:ToDoTask=new ToDoTask();
  editTask:boolean=false;

  constructor(private todoService: TodoServiceService) { }

  ngOnInit() {
  this.getToDoTasks();
  }

  getToDoTasks(){
    this.todoService.getToDoTasks().then(
      result=>
      {this.todoTasks=result
      console.log(this.todoTasks)
      }
    );
  }

  editTodoTask(todoTask: ToDoTask){
    console.log(todoTask);
  }

  deleteTodoTask(todoTask:ToDoTask){
    console.log(todoTask);
    this.todoService.deleteTask(todoTask)
    .then(() => {
      this.todoTasks = this.todoTasks.filter(task => task.taskID != todoTask.taskID);
    });

  }

  toggleCompleted(todoTask: ToDoTask)
  {
    console.log(todoTask);
    todoTask.pending=!todoTask.pending;
    this.todoService.updateTodo(todoTask)
    .then(() => {
      result=>
      {
        console.log(result);
        this.todoTasks=result
        console.log(this.todoTasks)
      }
    });

  }

  create(todoForm: NgForm): void {
    let localtodoTask:ToDoTask=new ToDoTask();
    if(this.todoTasks)
    localtodoTask.taskID= this.todoTasks.length+1;
    else 
    localtodoTask.taskID= 1
    localtodoTask.taskDescription=this.todoTask.taskDescription;
    localtodoTask.pending=true;
    console.log(this.todoTask);
    console.log(localtodoTask);
    this.todoService.createTodo(localtodoTask)
    .then(
      createTodo => { 
      console.log(createTodo); 
      todoForm.reset();
      this.todoTask = new ToDoTask();
      if(!this.todoTasks)
        this.todoTasks=new Array();
      this.todoTasks.unshift(localtodoTask)
      
      

    });
  }


}
